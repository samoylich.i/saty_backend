from rest_framework import serializers
from django.db import IntegrityError
from rest_framework_gis.serializers import GeoFeatureModelSerializer
from .models import Event
from apps.satellite_images_searcher.models import SatelliteImage
import json


class EventWriteDataSerializer(serializers.ModelSerializer):
    event_url = serializers.URLField(required=False)

    class Meta:
        model = Event
        fields = ('title',
                  'event_type',
                  'place',
                  'event_lon',
                  'event_lat',
                  'start_date',
                  'end_date',
                  'event_url',
                  'affected_area'
                  )

    def create_or_update(self, *args, **kwargs):
        try:
            event, created = Event.objects.update_or_create(
                event_url=self.validated_data['event_url'], defaults=self.validated_data)
            return event, created
        except IntegrityError:
            return Event.objects.get(event_url=self.validated_data['event_url']), False


class EventReadDataSerializer(serializers.ModelSerializer):
    check_sat = serializers.SerializerMethodField(read_only=True)
    affected_area = serializers.SerializerMethodField(read_only=True)
    size = serializers.SerializerMethodField(read_only=True)

    def get_size(self, obj):
        if obj.affected_area is None:
            return 0
        return obj.affected_area.area

    def get_affected_area(self, obj):
        if obj.affected_area is None:
            return None
        return json.loads(obj.affected_area.geojson)


    def get_check_sat(self, obj):
        return True if SatelliteImage.objects.filter(event=obj).count() >= 2 else False

    class Meta:
        model = Event
        fields = ('id',
                  'approved_status',
                  'title',
                  'event_type',
                  'place',
                  'adding_date',
                  'start_date',
                  'end_date',
                  'event_lat',
                  'event_lon',
                  'event_url',
                  'event_point',
                  'affected_area',
                  'size',
                  'check_sat')
        read_only_fields = ('id',
                            'approved_status',
                            'adding_date',
                            'start_date',
                            'end_date',
                            'event_lat',
                            'event_lon',
                            'event_url',
                            'event_point',
                            'affected_area',
                            'popularity',
                            'size',
                            "check_sat")



