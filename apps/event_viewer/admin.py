from django.contrib.gis import admin
from rangefilter.filter import DateRangeFilter
from django.utils.html import format_html
from .models import Event
from apps.satellite_images_searcher.search_scene import search_satellite_scene, _get_queryset
from apps.satellite_images_searcher.models import SatelliteImage
from .additional_functions import return_address
from django.contrib.gis.geos import GEOSGeometry


class EventAdmin(admin.OSMGeoAdmin):

    def save_model(self, request, obj, form, change):
        obj.change_lon_lat_by_point()
        super(EventAdmin, self).save_model(request, obj, form, change)

    def load_new_sat_images(self, request, queryset):
        for obj in queryset:
            search_satellite_scene(str(obj.start_date),
                                   obj.event_lon,
                                   obj.event_lat,
                                   obj)


    def sat_im_count(self, obj):
        count_sat = SatelliteImage.objects.filter(event=obj).count()
        return format_html(
            '<a class="button" href="/admin/' +
            'satellite_images_searcher/satelliteimage/?event__pk={pk}">Count:{count}</a>',
            pk=obj.pk, count=str(count_sat))


    def change_place(self, request, objects):
        for obj in objects:
            obj.place = return_address(obj.event_point.get_y(), obj.event_point.get_x())
            obj.save()

    def add_more_sat_images(self, request, objects):
        for obj in objects:
            for satellite in ["landsat-8", "Sentinel-2"]:
                try:
                    last_date = SatelliteImage.objects.filter(
                        satellite_name=satellite, event=obj).order_by("date").last().date.strftime('%Y-%m-%d')
                except AttributeError:
                    last_date = str(obj.start_date)
                response_images = _get_queryset(last_date, obj.event_lon, obj.event_lat, satellite, 1)
                for image in response_images:
                    if not SatelliteImage.objects.filter(
                            event=obj, scene_name=image["scene_name"]):
                        SatelliteImage.objects.create(
                            event=obj,
                            cloud_coverage=image["cloud_coverage"],
                            satellite_name=image["satellite_name"],
                            scene_id=image["scene_id"],
                            scene_name=image["scene_name"],
                            date=image["date"],
                            image_url=image["image_url"],
                            data_geometry=GEOSGeometry(image["data_geometry"]))




    sat_im_count.allow_tags = True
    sat_im_count.short_description = "Count sat"
    save_on_top = True
    load_new_sat_images.short_description = 'Load new data to sat images'
    change_place.short_descriptions = "Change event place"
    date_hierarchy = 'adding_date'
    actions = [
               load_new_sat_images, change_place, add_more_sat_images]
    list_display = (
        '__str__',
        'place',
        'event_type',
        'start_date',
        'adding_date',
        'sat_im_count',
        'approved_status',)
    list_filter = (
        'approved_status',
        ('start_date',
         DateRangeFilter),
        'event_type'
    )
    ordering = ('-adding_date',)
    search_fields = ['title', 'place', 'start_date']





admin.site.register(Event, EventAdmin)


