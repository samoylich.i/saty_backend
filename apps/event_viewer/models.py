import time
from django.contrib.gis.db import models
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point
from django.utils import timezone

TYPES = ['Wildfire']
EVENT_TYPE = sorted((etype, etype) for etype in TYPES)


class Event(models.Model):
    approved_status = models.BooleanField(default=False, blank=True)
    title = models.CharField(max_length=100)
    event_type = models.CharField(
        choices=EVENT_TYPE, default='Wildfire', max_length=50)
    place = models.CharField(default='California', max_length=256)
    adding_date = models.DateField(auto_now_add=True)
    start_date = models.DateField()
    end_date = models.DateField()
    event_lat = models.FloatField(default=0.00)
    event_lon = models.FloatField(default=0.00)
    event_url = models.URLField(unique=True)
    event_point = models.PointField(default=Point(0.00, 0.00, srid=4326))
    affected_area = models.MultiPolygonField(blank=True, null=True, srid=4326)
    size = models.IntegerField(default=0, blank=True, null=True)

    class Meta:
        ordering = ('-start_date', '-pk')

    def __init__(self, *args, **kwargs):
        super(Event, self).__init__(*args, **kwargs)
        self.event_point = self.get_coord_point()

    def __str__(self):
        return str(self.pk) + ' ' + str(self.title) + \
            ' ' + str(self.event_type)

    def change_lon_lat_by_point(self):
        self.event_lon = self.event_point.get_x()
        self.event_lat = self.event_point.get_y()

    def get_coord_point(self):
        return Point(self.event_lon, self.event_lat,  srid=4326)




