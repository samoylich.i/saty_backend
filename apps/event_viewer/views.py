import re

from django.http import JsonResponse
from django.contrib.gis.geos import GEOSGeometry
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.status import (HTTP_200_OK,
                                   HTTP_201_CREATED,
                                   HTTP_400_BAD_REQUEST)
from rest_framework.views import APIView

from .models import Event, TYPES
from .serializers import (EventWriteDataSerializer, EventReadDataSerializer)
from apps.satellite_images_searcher.search_scene import search_satellite_scene
from apps.event_viewer.additional_functions import event_filter, get_event_pagination


class EventList(generics.ListCreateAPIView):

    def get_queryset(self):
        if self.request.user.is_staff:
            return Event.objects.all()
        else:
            return Event.objects.filter()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return EventWriteDataSerializer
        return EventReadDataSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            event, created = serializer.create_or_update()
            search_satellite_scene(
                str(serializer.validated_data['start_date']),
                str(serializer.validated_data["event_lon"]),
                str(serializer.validated_data["event_lat"]),
                event)
            if created:
                return Response(serializer.data, status=HTTP_201_CREATED)
            else:
                return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class EventDetail(generics.RetrieveUpdateDestroyAPIView):

    def get_queryset(self):
        if self.request.user.is_staff:
            return Event.objects.all()
        else:
            return Event.objects.filter()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return EventWriteDataSerializer
        return EventReadDataSerializer

    def put(self, request, *args, **kwargs):
        event = self.get_object()
        serializer = self.get_serializer(event, data=request.data)
        if serializer.is_valid():
            serializer.create_or_update()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class EventManyFiltering(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = EventReadDataSerializer

    def custom_get_queryset(self, user):
        if user.is_staff:
            self.queryset = Event.objects.all()
        else:
            self.queryset = Event.objects.filter()
        return self.queryset

    def post(self, request):
        queryset, error = event_filter(request=request.data,
                                       queryset=self.custom_get_queryset(request.user))
        if error:
            return Response(error, status=HTTP_400_BAD_REQUEST)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class GetPaginationById(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        queryset = EventManyFiltering().custom_get_queryset(request.user)
        filtered_queryset, error = event_filter(request=request.data,
                                                queryset=queryset)
        if error:
            return Response(error, status=HTTP_400_BAD_REQUEST)
        if request.data.get('event_id'):
            page = get_event_pagination(filtered_queryset, request.data.get('event_id'))
            if page:
                return Response(page, status=HTTP_200_OK)
        return Response(status=HTTP_400_BAD_REQUEST)


def event_types(request):
    resp_data = ({'event_types': TYPES})
    return JsonResponse(resp_data)
