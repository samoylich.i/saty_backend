from django.apps import AppConfig


class EventViewerConfig(AppConfig):
    name = 'apps.event_viewer'
    verbose_name = 'Event Viewer'
