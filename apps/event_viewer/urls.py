from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from . import views


schema_view = get_swagger_view(title='Event Service API')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^events/$', views.EventList.as_view(),
        name='events-list'),
    url(r'^events/(?P<pk>\d+)/$', views.EventDetail.as_view(),
        name='events-detail'),
    url(r'^events/many_filter/$',
        views.EventManyFiltering.as_view(),
        name='events-filter'),
    url(r'^event-id/$', views.GetPaginationById.as_view(),
        name='event-page'),
    url(r'^event-types/$', views.event_types,
        name='types'),
    ]
