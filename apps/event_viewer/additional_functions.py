import logging
import requests
import math
import re

from django.contrib.gis.gdal import GDALException
from django.contrib.gis.geos import GEOSGeometry, GEOSException
from rest_framework.settings import api_settings

from apps.event_viewer.models import Event
from apps.satellite_images_searcher.search_scene import search_satellite_scene
from .serializers import EventWriteDataSerializer


def save_or_update_event(data):
    """
    :param data: event values dict
    :return: creating new event or update by event url
    """
    serializer = EventWriteDataSerializer(data=data)
    if serializer.is_valid():
        event, created = serializer.create_or_update()
        search_satellite_scene(
            str(event.start_date),
            event.event_lon,
            event.event_lat,
            event)


def event_filter(request, queryset):
    error = None
    if request.get('title'):
        queryset = queryset.filter(title__icontains=request['title'])

    if request.get('place'):
        queryset = queryset.filter(place__icontains=request['place'])

    if request.get('event_type'):
        try:
            queryset = queryset.filter(event_type__in=request['event_type'].split('&'))
        except(AttributeError, ValueError):
            error = "Enter valid event types data"
    regular = r'[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'
    if request.get('start_point_date') and request.get('end_point_date'):
        if re.match(regular, request['start_point_date']) and re.match(regular,
                                                                       request['end_point_date']):
            queryset = queryset.filter(start_date__range=(request['start_point_date'],
                                                          request['end_point_date']))
    if request.get("polygon"):
        try:
            polygon = GEOSGeometry(str(request['polygon']))
            queryset = queryset.filter(event_point__intersects=polygon)
        except (GEOSException, ValueError, GDALException):
            error = "Enter valid polygon data"
    if request.get('popularity'):
        queryset = queryset.order_by('-popularity', '-start_date', '-pk')
    return queryset, error


def get_event_pagination(filtered_queryset, event_id):
    try:
        filtered_queryset.get(pk=event_id)
        pk_list = []
        for event in filtered_queryset:
            pk_list.append(event.pk)
        num = int(pk_list.index(event_id)) + 1
        page = math.ceil(num / api_settings.PAGE_SIZE)
        return page
    except (Event.DoesNotExist):
        return None


def return_address(lat, lon):
    try:
        data = requests.get("https://osm.eos.com/nominatim/reverse?format=json&lat=" + str(lat)  + "&lon=" + str(lon) + "&accept-language=en-US").json()
        address = data["address"]
        country = address["country"]
        if country in ['Albania', 'Andorra', 'Barbados', 'Bermuda', 'Bulgaria', 'Cape Verde', 'Croatia', 'Cyprus', 'Estonia', 'Falkland Islands', 'Germany', 'Ireland', 'Liechtenstein',
                       'Luxembourg', 'Moldova', 'Montenegro', 'Montserrat', 'Morocco', 'Morocco', 'Romania', 'Seychelles', 'Slovenia', 'Taiwan', 'Brunei']:
            return address['county'] + ", " + address['country']
        elif country == 'United States of America':
            return address['county'] + ', ' + address['state'] + ', ' + 'USA'
        elif country in ['Afghanistan', 'United Kingdom']:
            return address['county'] + ', ' +  address['state'] + ', ' +  address['country']
        elif country in ['Algeria', 'Angola', 'Argentina', 'Australia', 'Austria', 'Bahrain', 'Belarus', 'Belgium',
                         'Belize', 'Benin', 'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Burundi', 'Cambodia',
                         'Cameroon', 'Canada', 'Central African Republic', 'Chad', 'Chile', 'China', 'China',
                         'Colombia', 'Colombia', 'Comoros', 'Costa Rica', 'Cuba', 'Czechia', "Côte d'Ivoire",
                         'DR Congo', 'Denmark', 'Djibouti', 'Dominica', 'East Timor', 'Ecuador', 'Egypt', 'El Salvador',
                         'Equatorial Guinea', 'Eritrea', 'Finland', 'France', 'Gabon', 'Georgia', 'Ghana', 'Guatemala',
                         'Guinea', 'Guinea-Bissau', 'Guyana', 'Haiti', 'Honduras', 'India', 'Indonesia', 'Iran', 'Iraq',
                         'Israel', 'Italy', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kuwait', 'Kyrgyzstan', 'Laos',
                         'Latvia', 'Lebanon', 'Liberia', 'Libya', 'Lithuania', 'Macedonia', 'Madagascar', 'Malawi',
                         'Malaysia', 'Maldives', 'Mali', 'Mauritania', 'Mauritius', 'Mexico', 'Mongolia', 'Mozambique',
                         'Myanmar', 'Namibia', 'Nepal', 'New Zealand', 'Nicaragua', 'Nigeria', 'North Korea', 'Norway',
                         'Oman', 'Pakistan', 'Panama', 'Paraguay', 'Peru', 'Philippines', 'Poland',
                         'Portugal', 'Qatar', 'RSA', 'Russia', 'Rwanda', 'Saudi Arabia', 'Senegal', 'Serbia',
                         'Sierra Leone', 'Slovakia', 'Solomon Islands', 'Somalia', 'South Korea', 'South Sudan',
                         'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria',
                         'Tajikistan', 'Tanzania', 'Territorial waters of Faroe Islands', 'Thailand', 'The Netherlands',
                         'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Uganda',
                         'Ukraine', 'United Arab Emirates', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela', 'Vietnam',
                         'Yemen', 'Zambia', 'Zimbabwe', 'Armenia', 'Azerbaijan', 'Bangladesh', 'Federated States of Micronesia', 'Greece']:
            return address['state'] + ", " + address['country']
        elif country in ['Burkina Faso', 'Hungary', 'Niger']:
            return address['region'] + ", " + address['country']
        elif country in ['Anguilla', 'Antigua and Barbuda', 'British Indian Ocean Territory', 'British Virgin Islands',
                         'Cayman Islands', 'Cook Islands', 'Gibraltar', 'Greenland', 'Grenada', 'Guernsey', 'Iceland',
                         'Isle of Man', 'Jersey', 'Kiribati', 'Marshall Islands', 'Nauru', 'Niue', 'Palau',
                         'Saint Helena, Ascension and Tristan da Cunha', 'Saint Kitts and Nevis', 'Saint Lucia',
                         'Saint Vincent and the Grenadines', 'Samoa', 'San Marino', 'Singapore', 'São Tomé and Príncipe',
                         'Territorial waters of Faroe Islands', 'Tokelau', 'Turks and Caicos Islands', 'Tuvalu',
                         'Vatican City', 'The Bahamas', 'Monaco']:
            return address['country']
        elif country in ['Jamaica', 'Lesotho', 'Gambia']:
            return address['state_district'] + ", " + address['country']
        elif country in ['Malta']:
            return address['town'] + ", " + address['country']
        elif country in ['Myanmar']:
            return address['county'] + ", " +  address['state_district'] + ", " + address['country']
        elif country == "D.R.":
            return address['state'] + ", " + "Dominican Republic"
        elif country in ["Fiji", 'Papua New Guinea']:
            return address['state'] + " State, " + address['country']
        else:
            return data['display_name']
    except KeyError:
        try:
            if address.get("state") == 'Autonomous Republic of Crimea':
                return 'Autonomous Republic of Crimea'
        except UnboundLocalError:
            return "Undefined place"
        return "Undefined place"
