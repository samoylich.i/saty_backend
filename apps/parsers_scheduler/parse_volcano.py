import json
import datetime
from bs4 import BeautifulSoup
import requests
from selenium import webdriver
from apps.event_viewer.additional_functions import save_or_update_event
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from requests.exceptions import RequestException
from apps.event_viewer.additional_functions import return_address


def get_html(url):
    return requests.get(url).text


def convert_date(date):
    return datetime.datetime.strptime(date, '%Y %b %d').strftime('%Y-%m-%d')


def parse_volcano_si_edu(count_checked_volcano=0):
    start_url = 'http://volcano.si.edu/'
    site = 'http://volcano.si.edu/search_eruption.cfm'
    driver = webdriver.Remote(command_executor='http://phantomjs:8910', desired_capabilities=DesiredCapabilities.PHANTOMJS)
    driver.get(site)
    driver.delete_all_cookies()
    driver.find_element_by_xpath("//input[@id='submit_account']").click()
    driver.delete_all_cookies()
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    for i in soup.find_all('div', {"class": 'tr'}):
        parse_for_all_event(i, start_url)
        count_checked_volcano += 1
        if count_checked_volcano == 10:
            break


def parse_for_all_event(wrapper, start_url):
    try:
        wrapper = BeautifulSoup(wrapper, "html.parser")
        temp_start_date = str(wrapper).split('\n<div class="td3">')[2].split('</div>')[0]
        start_date = convert_date(temp_start_date)

        url_this_volcano = start_url + wrapper.a.get('href').split('..')[1]

        title = wrapper.a.get_text()

        soup_concrete_volcano = BeautifulSoup(get_html(url_this_volcano), 'html.parser')
        #### on page concrete volcano

        text_with_coord = soup_concrete_volcano.find('div', {'class': 'volcano-subinfo-table'}).ul.get_text()
        lat = float(text_with_coord.split('°')[0].split('\n')[1])

        if text_with_coord.split('°')[1].split('\n')[0] == 'S':
            lat = lat * -1

        if text_with_coord.split('°')[2].split('\n')[0] == 'W':
            long = float(text_with_coord.split('°')[1].split('\n')[1]) * -1
        else:
            long = float(text_with_coord.split('°')[1].split('\n')[1])
        place = return_address(lat, long)

        # print(str(long)+' '+str(lat))

        ####

        data = {'title': title,
                'event_lon': long,
                'event_lat': lat,
                'event_type': 'Volcano',
                'start_date': start_date,
                'end_date': start_date,
                'type': 'Volcano',
                "tweet_query": '"Eruption ' + title + '" since:' + start_date + ' -filter:retweets',
                'event_url': url_this_volcano,
                'place': place,
                "size": 0,
                "affected_area": {
                    "type": "MultiPolygon",
                    "coordinates": []
                },
                }

        print(json.dumps(data))
        save_or_update_event(data)
    except RequestException:
        pass
