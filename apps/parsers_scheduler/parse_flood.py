from bs4 import BeautifulSoup
import requests
import datetime
import urllib.request
import json
from apps.event_viewer.additional_functions import save_or_update_event
from apps.event_viewer.additional_functions import return_address

def parse_flood():
    url = 'http://www.gdacs.org/xml/rss_fl_3m.xml'
    try:
        soup = BeautifulSoup(requests.get(url).text)
        for item in soup.find_all('item'):
            parse_flood_for_all_item(str(item))
    except ConnectionError:
        pass


def _get_area(url_Json):
    with urllib.request.urlopen(url_Json) as url:
        return json.loads(url.read().decode())['features'][1]['geometry']['coordinates']


def _convert_date(date):
    return (datetime.datetime.strptime(date, '%a, %d %b %Y %H:%M:%S %Z')).strftime('%Y-%m-%d')


def parse_flood_for_all_item(soup):
    soup = BeautifulSoup(soup, 'html.parser')
    title = soup.title.get_text()
    start_date = soup.find('gdacs:fromdate').get_text()
    end_date = soup.find('gdacs:todate').get_text()
    lat = soup.find('geo:lat').get_text()
    long = soup.find('geo:long').get_text()
    place = return_address(float(lat), float(long))
    try:
        link_json_coord = _get_area(
            str(soup.find('gdacs:resource', {'id': "event_geojson"})).split('url')[1].split('"')[1])
    except:
        link_json_coord = []
    link = str(soup).split('<link/>')[1].split('\r')[0]
    title = title.replace('Green flood alert in', 'Flood').replace(
        'White flood alert in', 'Flood').replace('|', ' ')
    data = {'title': title,
            'event_lon': float(long),
            'event_lat': float(lat),
            'event_type': 'Flood',
            'start_date': _convert_date(start_date),
            'end_date': _convert_date(end_date),
            "tweet_query": '"' + title + '" since:' + _convert_date(start_date) + ' -filter:retweets',
            "affected_area": {
                "coordinates": link_json_coord,
                "type": "MultiPolygon"
            },
            'event_url': link,
            'place': place,
            "size": 0,
            }
    save_or_update_event(data)
