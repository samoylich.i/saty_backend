from datetime import datetime
import requests
from dateutil import parser
from apps.event_viewer.additional_functions import save_or_update_event
from django.contrib.gis.geos import GEOSGeometry
from apps.event_viewer.additional_functions import return_address

def parsing_all_eonet_events():
    categories = ["6", "9", "14", "19", "12", "8"]
    event_types = {'Drought':'Drought',
                   'Floods': 'Flood',
                   'Landslides': 'Landslide',
                   'Manmade': 'Manmade',
                   'Water Color': 'Water Color',
                   'Volcanoes': 'Volcano',
                   'Wildfires': 'Wildfire'}
    url = "https://eonet.sci.gsfc.nasa.gov/api/v2.1/categories/"
    for cat in categories:
        req_data = requests.get(url+cat + "?limit=10").json()
        for event in req_data["events"]:
            start_date = parser.parse(event["geometries"][0]["date"])
            event_type = event_types[event["categories"][0]['title']]
            if event["geometries"][0]["type"] == "Polygon":
                event["geometries"][0].pop('date')
                multipolygon = GEOSGeometry(str(event["geometries"][0]))
                center = multipolygon.centroid.coords
                lon = center[0]
                lat = center[1]
                area = [event["geometries"][0]["coordinates"]]
            elif event["geometries"][0]["type"] == "Point":
                lon = event["geometries"][0]["coordinates"][0]
                lat = event["geometries"][0]["coordinates"][1]
                area = []
            unnecessary_sources = False
            for source in event["sources"]:
                if ("inciweb" in source["url"]) or ("gdacs" in source["url"]):
                    unnecessary_sources = True
                    break
            if unnecessary_sources:
                continue
            place = return_address(lat, lon)
            if event_type=="Wildfire":
                event["title"] = "Fire in " + place.split(",")[0]
            data = {'title': event["title"],
                    'place': place,
                    'event_lon': lon,
                    'event_lat': lat,
                    'event_type': event_type,
                    'start_date': start_date.strftime('%Y-%m-%d'),
                    'approved_status': True,
                    'end_date': datetime.now().date().strftime('%Y-%m-%d'),
                    "tweet_query": '"' + event["title"] + '" since:' + start_date.strftime('%Y-%m-%d') + ' -filter:retweets',
                    'event_url': event["sources"][0]['url'],
                    "affected_area": {"type": "MultiPolygon",
                                    "coordinates": area}
                    }
            save_or_update_event(data)
