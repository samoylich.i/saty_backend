from bs4 import BeautifulSoup
import requests
import geocoder
from dateutil import parser
from django.core.exceptions import ObjectDoesNotExist
from apps.event_viewer.additional_functions import save_or_update_event
from apps.event_viewer.models import Event
from apps.event_viewer.additional_functions import return_address


def parsing_inciweb():
    url = 'https://inciweb.nwcg.gov/feeds/rss/incidents/'
    try:
        page = requests.get(url).text
        soup = BeautifulSoup(page, 'html.parser')
        for wrapper in soup.find_all('item'):
            new_link = wrapper.find('guid').get_text()
            parse_for_all_item(wrapper, new_link)
            search_photo(new_link)
    except BaseException:
        pass


def search_photo(start_url_event):
    try:
        event = Event.objects.get(event_url=start_url_event)
        url = start_url_event.replace('/incident/', '/incident/photographs/')
        soup = BeautifulSoup(requests.get(url).text, 'html.parser')
        for wrapper in soup.find_all('img'):
            photo = wrapper['src']
            if photo.__contains__('http://inciweb.nwcg.gov/photos'):
                try:
                    ImageModel.objects.get(event=event, image_url=photo)
                except ObjectDoesNotExist:
                    ImageModel.objects.create(event=event,
                                              image_url=photo,
                                              news_url=start_url_event)
    except (BaseException):
        pass


def _convert_date(date):
    date = date.replace('st,', '').replace(
        'nd,', '').replace('rd,', '').replace('th,', '')
    # datetime.datetime.strptime(date, '%A %B %d %Y')
    parsed_date = parser.parse(date.split('approx.')[0])
    return parsed_date


def parse_for_all_item(wrapper, new_link):
    wrapper = BeautifulSoup(str(wrapper), "html.parser")
    title = wrapper.find('title').get_text()
    if title.__contains__('(Wildfire)'):
        try:
            title = title.replace(' (Wildfire)', '')
            lat = wrapper.find('geo:lat').get_text()
            lon = wrapper.find('geo:long').get_text()
            new_soup = BeautifulSoup(requests.get(new_link).text, 'html.parser')
            start_date = _convert_date(
                (new_soup.find('td', string='Date of Origin').nextSibling.get_text()))
            try:
                end_date = _convert_date((new_soup.find(
                    'td', string='Estimated Containment Date').nextSibling.get_text()))
            except BaseException:
                end_date = start_date
            try:
                try:
                    str_size = (new_soup.find('td', string='Size').nextSibling.get_text()).split()[0]
                    size = int(str_size)
                except ValueError:
                    size = int("".join(str_size.split(',')))
            except BaseException:
                size = 0

            place = return_address(float(lat), float(lon))
            data = {'title': title,
                    'place': place,
                    'event_lon': float(lon),
                    'event_lat': float(lat),
                    'event_type': 'Wildfire',
                    'start_date': start_date.strftime('%Y-%m-%d'),
                    'approved_status': True,
                    'end_date': end_date.strftime('%Y-%m-%d'),
                    "tweet_query": '"' + title + '" since:' + start_date.strftime('%Y-%m-%d') + ' -filter:retweets',
                    'event_url': new_link,
                    "affected_area": {
                        "type": "MultiPolygon",
                        "coordinates": [],
                    },
                    "size": size,
                    }
            if data is not None:
                save_or_update_event(data)
        except BaseException:
            pass
