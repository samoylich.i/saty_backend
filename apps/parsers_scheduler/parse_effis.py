import math
import requests
from bs4 import BeautifulSoup
import datetime
from apps.event_viewer.additional_functions import save_or_update_event
from apps.event_viewer.additional_functions import return_address

URl_EFFIS = 'http://effis.jrc.ec.europa.eu/'


def parse_effis(days):
    days = int(days)
    finish = datetime.datetime.now().strftime('%Y/%m/%d')
    start = (datetime.datetime.now() -
             datetime.timedelta(days=days)).strftime('%Y/%m/%d')
    url = 'http://effis.jrc.ec.europa.eu/applications/fire-news/?q=&from_date=' + \
        start + '&to_date=' + finish
    try:
        soup = BeautifulSoup(requests.get(url).text, 'html.parser')
        for link in soup.find_all('a'):
            parse_for_all_link(str(link))
    except ConnectionError:
        pass


def _meters_to_degress(x, y):
    lon = x * 180 / 20037508.34
    lat = math.atan(math.exp(y * math.pi / 20037508.34)) * 360 / math.pi - 90
    return [lon, lat]


def _convert_date(date):
    return datetime.datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')


def parse_for_all_link(link):
    try:
        if str(link).__contains__('/applications/fire-news/fire/'):
            short_url = str(link).split('<a href="')[1].split('">')[0]
            new_soup = BeautifulSoup(requests.get(
                URl_EFFIS + short_url).text, 'html.parser')
            for wrap in new_soup.find('div', {'class': 'col-md-12'}).find_all('li'):
                if str(wrap).__contains__('<li>Start Date:'):
                    start_date = str(wrap).split('<li>Start Date: ')[
                        1].split('</li>')[0]

                if str(wrap).__contains__('<li>End Date:'):
                    finish_date = str(wrap).split('<li>End Date: ')[
                        1].split('</li>')[0]
            coord_in_meters = new_soup.find('textarea').get_text().split(
                'POINT (')[1].split(')')[0].split()
            coord_in_degress = _meters_to_degress(
                float(coord_in_meters[0]), float(coord_in_meters[1]))
            place = return_address(float(coord_in_degress[1]),
                                   float(coord_in_degress[0]))
            data = {'title': str('Fire ' + place),
                    'event_lon': float(coord_in_degress[0]),
                    'event_lat': float(coord_in_degress[1]),
                    'event_type': str('Wildfire'),
                    'start_date': str(_convert_date(start_date)),
                    'end_date': str(_convert_date(finish_date)),
                    'event_url': str(URl_EFFIS + short_url),
                    "size": 0,
                    'place': str(place),
                    "tweet_query": '"' + str('wildfire ' + place) + '" since:' + str(
                        _convert_date(start_date)) + ' -filter:retweets',
                    "affected_area": {
                        "type": "MultiPolygon",
                        "coordinates": []
            },
            }
            if data is not None:
                save_or_update_event(data)
    except (AttributeError, ConnectionError):
        pass
