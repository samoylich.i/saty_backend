from bs4 import BeautifulSoup
import requests
import datetime
import urllib.request
from urllib.error import URLError
import json
from apps.event_viewer.additional_functions import save_or_update_event
from apps.event_viewer.additional_functions import return_address


def _get_xml(url):
    return requests.get(url).text


def _convert_date(date):
    temp = date.split(' ')[0]
    return (datetime.datetime.strptime(temp, '%d/%b/%Y')).strftime('%Y-%m-%d')


def parse_earthquakes():
    url = 'http://www.gdacs.org/xml/rss_eq_5.5_3m.xml'
    try:
        soup = BeautifulSoup(_get_xml(url))
        for i in soup.find_all('item'):
            link = str(i.find('gdacs:resource', {'id': "event_geojson"})).split(
                'url')[1].split('"')[1]
            try:
                with urllib.request.urlopen(link) as url:
                    json_data = json.loads(url.read().decode("utf-8"))
                    parse_date_from_json(json_data)
            except URLError:
                pass
    except ConnectionError:
        pass


def parse_date_from_json(json_data):
    title = json_data['features'][-1]['properties']['name']
    link = json_data['features'][-1]['properties']['link']
    start_date = json_data['features'][-1]['properties']['fromdate']
    end_date = json_data['features'][-1]['properties']['todate']
    lat = json_data['features'][-1]['properties']['latitude']
    long = json_data['features'][-1]['properties']['longitude']
    coord = json_data['features'][1]['geometry']['coordinates']
    title = title.replace('in ', '')
    place = return_address(float(lat), float(long))

    data = {'title': str(title),
            'event_lon': float(long),
            'event_lat': float(lat),
            'event_type': 'Earthquake',
            'start_date': _convert_date(start_date),
            'end_date': _convert_date(end_date),
            'event_url': str(link),
            "affected_area": {
                "coordinates": coord,
                'type': "MultiPolygon",
            },
            "size": 0,
            "tweet_query": '"' + title + '" since:' + _convert_date(start_date) + ' -filter:retweets',
            'place': str(place),
            }
    save_or_update_event(data)
