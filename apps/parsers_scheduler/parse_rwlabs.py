import requests
import json
from apps.event_viewer.additional_functions import save_or_update_event
from apps.event_viewer.additional_functions import return_address


def parse_rwlabs():
    url_rwlabs = 'https://api.reliefweb.int/v1/disasters?appname=test'

    event_types = ['Cyclone', "Fire", "Tropical Cyclone", 'Wild Fire']

    for new_event_type in event_types:
        params = {
            'limit': '4',
            'preset': 'latest',
            'profile': 'full',
            "filter":
                {
                    'field': 'primary_type.name',
                    'value': new_event_type,
                    "operator": "AND",
                },
        }
        response = requests.post(url=url_rwlabs, data=json.dumps(params))
        for event in (response.json()['data']):
            parse_for_all_event(event)


def parse_for_all_event(event):
    title = str(event['fields']['name'])
    event_type = str(event['fields']['primary_type']['name'])
    start_date = str(event['fields']['date']['created'])[:10]
    end_date = start_date
    event_lat = str(event['fields']['primary_country']['location']['lat'])
    event_lon = str(event['fields']['primary_country']['location']['lon'])
    place = return_address(float(event_lat), float(event_lon))
    event_url = str(event['fields']['url'])

    if event_type in ['Wild Fire', 'Fire']:
        event_type = 'Wildfire'
    if event_type in ['Cyclone', "Tropical Cyclone"]:
        event_type = 'Cyclone'
    if event_type in ['Flood', 'Flash Flood']:
        event_type = 'Flood'

    data = {
        'title': title,
        'event_type': event_type,
        'place': place,
        'start_date': start_date,
        'end_date': end_date,
        'event_lat': float(event_lat),
        'event_lon': float(event_lon),
        'tweet_query': str(place) + " " + str(event_type) + ' ' + 'since:' + str(start_date) + ' -filter:retweets',
        'event_url': event_url,
        "affected_area": {
            "type": "MultiPolygon",
            "coordinates": []
        },
        "size": 0
    }
    save_or_update_event(data)
    return data
