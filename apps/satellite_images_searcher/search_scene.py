import json
import requests
import urllib
from datetime import datetime, timedelta
from .models import SatelliteImage
from django.contrib.gis.geos import GEOSGeometry
from dateutil import parser


def search_satellite_scene(start_date, lon, lat, event):
    for satellite in ["modis", "landsat-7", "landsat-8",
                      "Sentinel-2", "Sentinel-1", "CBERS-4"]:
        for direction in range(2):
            response_images = _get_queryset(start_date, lon, lat,
                                            satellite, direction)
            if satellite == "modis" and direction == 1:
                response_images = response_images[-6:]
            elif satellite == "modis" and direction == 0:
                response_images = response_images[:7]
            for image in response_images:
                if not SatelliteImage.objects.filter(
                        event=event, scene_name=image["scene_name"]):
                    SatelliteImage.objects.create(
                        event=event,
                        cloud_coverage=image["cloud_coverage"],
                        satellite_name=image["satellite_name"],
                        scene_id=image["scene_id"],
                        scene_name=image["scene_name"],
                        date=image["date"],
                        image_url=image["image_url"],
                        data_geometry=GEOSGeometry(image["data_geometry"]))


def _get_queryset(current_date, lon, lat, satellite, direction):
    if int(direction):
        start_date = current_date
        end_date = (
            datetime.strptime(
                current_date,
                "%Y-%m-%d") +
            timedelta(30)).date()
    else:
        start_date = (
            datetime.strptime(
                current_date,
                "%Y-%m-%d") -
            timedelta(30)).date()
        end_date = (
            datetime.strptime(
                current_date,
                "%Y-%m-%d") -
            timedelta(1)).date()
    search_data = {
        "search": {
            "date": {
                "from": str(start_date),
                "to": str(end_date)
            },
            "shape": {
                "type": "Point",
                "coordinates": [
                    float(lon),
                    float(lat)
                ]
            }
        },
        "page": 1,
        "limit": 500,
        "fields": [
            "sceneID",
            "awsPath",
            "cloudCoverage",
            "satelliteName",
            "horizontalTileNumber",
            "verticalTileNumber",
            "BeginningDateTime",
            "productID",
            "date",
            "thumbnail",
            "dataGeometry"
        ]
    }
    sat_dict = {"landsat-8": "landsat8",
                "landsat-7": "landsat7",
                "Sentinel-2": "sentinel2",
                "Sentinel-1": "sentinel1",
                "modis": "modis",
                "CBERS-4": "cbers4"}
    response_data = json.loads(requests.post(
        "http://lms.eosda.com/search/v2/" + sat_dict[satellite],
        data=json.dumps(search_data)).content.decode("utf-8"))["results"]
    if satellite == "modis":
        for image in response_data:
            date = datetime.strptime(image["BeginningDateTime"],
                                     "%Y-%m-%dT%H:%M:%S").date()
            image.update({"date": str(date)})
    if satellite == "CBERS-4":
        for image in response_data:
            image["cloudCoverage"] = 0.0
    return_images = []
    try:
        images = [image for image in response_data if
                  (str(start_date) <= image["date"] <= str(end_date)) and
                  (0 <= image["cloudCoverage"] <= 85)]
    except KeyError:
        images = [image for image in response_data if
                  (str(start_date) <= image["date"] <= str(end_date))]
    for image in images:
        if sat_dict[satellite] in ("landsat7", "landsat8"):
            try:
                scene_id = scene_name = image["productID"]
            except KeyError:
                scene_id = scene_name = image["sceneID"]
            image_ulr = image["thumbnail"]
        elif sat_dict[satellite] == "sentinel2":
            scene_id = image['awsPath'][5:]
            scene_name = image['sceneID']
            image_ulr = "http://sentinel-s2-l1c.s3.amazonaws.com/" \
                        + image["awsPath"] + "/preview.jpg"
        elif sat_dict[satellite] == "sentinel1":
            scene_id = image['sceneID']
            scene_name = image['sceneID']
            image_ulr = "https://render.eosda.com/S1/thumb/" \
                        + scene_name + ".png"
            image["satelliteName"] = "Sentinel-1"
            image["cloudCoverage"] = 0.0
            image["date"] = parser.parse(
                image["date"]).date().strftime('%Y-%m-%d')
        elif sat_dict[satellite] == "modis":
            h_tile = int(images[0]["horizontalTileNumber"])
            if len(str(h_tile)) == 1:
                h_tile = "0" + str(h_tile)
            else:
                h_tile = str(h_tile)
            v_tile = int(images[0]["verticalTileNumber"])
            if len(str(v_tile)) == 1:
                v_tile = "0" + str(v_tile)
            else:
                v_tile = str(v_tile)
            scene_id = h_tile + "/" + v_tile + "/" + \
                image["sceneID"].split(".")[1][1:] + "/" + \
                image["sceneID"].split(".")[-1]
            scene_name = image['sceneID']
            image_ulr = image["thumbnail"]
        elif sat_dict[satellite] == "cbers4":
            scene_id = scene_name = image["sceneID"]
            image_ulr = "http://render.eosda.com/CBERS4/thumb/" + \
                        scene_id + ".jpeg"
            image["cloudCoverage"] = 0.0
        result_dict = {
            "cloud_coverage": image["cloudCoverage"],
            "satellite_name": image["satelliteName"],
            "scene_id": scene_id,
            "scene_name": scene_name,
            "date": image["date"],
            "image_url": image_ulr,
            "data_geometry": "{'type': 'Polygon', 'coordinates': " + str(
                image['dataGeometry']['coordinates']) + "}"}
        return_images.append(result_dict)
    return return_images


def _save_satimages_by_url(url, event):
    bands_dictionary = {"landsat-8": {"Coastal": "B1", "Blue": "B2",
                                      "Green": "B3", "Red": "B4",
                                      "NIR": "B5", "SWIR1": "B6",
                                      "SWIR2": "B7", "Panchrom": "B8",
                                      "Cirrus": "B9", "TIRS1": "B10",
                                      "TIRS2": "B11"},
                        "landsat-7": {"Blue": "B1", "Green": "B2",
                                      "Red": "B3", "NIR": "B4",
                                      "SWIR1": "B5", "Thermal2": "B6_VCID_2",
                                      "Thermal": "B6_VCID_1", "SWIR2": "B7",
                                      "Panchrom": "B8"},
                        "Sentinel-2A": {"Coastal": "B01", "Blue": "B02",
                                        "Green": "B03", "Red5": "B05",
                                        "Red6": "B06", "Red7": "B07",
                                        "Red8": "B8A", "Red": "B04",
                                        "NIR": "B08", "Water": "B09",
                                        "Cirrus": "B10", "SWIR1": "B11",
                                        "SWIR2": "B12"},
                        "Sentinel-2B": {"Coastal": "B01", "Blue": "B02",
                                        "Green": "B03", "Red5": "B05",
                                        "Red6": "B06", "Red7": "B07",
                                        "Red8": "B8A", "Red": "B04",
                                        "NIR": "B08", "Water": "B09",
                                        "Cirrus": "B10", "SWIR1": "B11",
                                        "SWIR2": "B12"},
                        "Sentinel-1": {"VV": "VV", "VH": "VH"},
                        "modis": {"Red": "B01", "NIR": "B02",
                                  "Blue": "B03", "Green": "B04",
                                  "SWIR1": "B06", "SWIR2": "B07",
                                  "SWIR": "B05"},
                        "CBERS-4": {"Blue": "BLUE", "Green": "GREEN",
                                    "Red": "RED", "NIR": "NIR"}}
    new_url = urllib.parse.unquote(urllib.parse.unquote(url))
    sat_values = {}
    for essence in new_url.split("?")[1].split("&"):
        try:
            sat_values[essence.split("=")[0]] = essence.split("=")[1]
        except IndexError:
            sat_values[essence] = True
    scene_to_sat = {"LC08": "Landsat8", "LE07": "Landsat7",
                    "S2": "Sentinel2", "S1": "Sentinel1", "MCD43A4": "Modis",
                    "CBERS_4_MUX": "CBERS4(MUX)",
                    "CBERS_4_AWFI": "CBERS4(AWFI)",
                    "CBERS_4_PAN10M": "CBERS4(PAN10M)",
                    "CBERS_4_PAN5M": "CBERS4(PAN5M)"}
    for key in scene_to_sat.keys():
        if sat_values["id"].find(key) != -1:
            sat_values["s"] = scene_to_sat[key]
        if sat_values.get("slider-id"):
            if sat_values["slider-id"].find(key) != -1:
                sat_values["slider-s"] = scene_to_sat[key]
    try:
        if sat_values.get("expression"):
            band_combination = sat_values["expression"]
        else:
            band_combination = sat_values["b"]
        more_data = ""
        if sat_values.get("colormap"):
            if sat_values.get("reverse"):
                more_data += "?COLORMAP=" + sat_values["colormap"] + "_r"
            else:
                more_data += "?COLORMAP=" + sat_values["colormap"]
            if sat_values.get("levels"):
                more_data += "&MIN_MAX=" + sat_values["levels"]
            if sat_values.get("discrete"):
                more_data += "&COLORS_LIMIT=" + sat_values["discrete"]
        if sat_values.get("pansharpening"):
            if sat_values["pansharpening"]:
                if more_data:
                    more_data += "&PANSHARPENING=True"
                else:
                    more_data += "?PANSHARPENING=True"
        land_viewer_data = [{"sat": sat_values["s"],
                             "band_c": band_combination,
                             "id": sat_values["id"],
                             "more": more_data}]
        if sat_values.get("side"):
            if sat_values.get("slider-expression"):
                band_combination_slider = sat_values["slider-expression"]
            else:
                band_combination_slider = sat_values["slider-b"]
            more_data_slider = ""
            if sat_values.get("slider-colormap"):
                if sat_values.get("slider-reverse"):
                    more_data_slider += "?COLORMAP=" \
                                        + sat_values["slider-colormap"] + "_r"
                else:
                    more_data_slider += "?COLORMAP=" \
                                        + sat_values["slider-colormap"]
                if sat_values.get("slider-levels"):
                    more_data_slider += "&MIN_MAX=" \
                                        + sat_values["slider-levels"]
                if sat_values.get("slider-discrete"):
                    more_data += "&COLORS_LIMIT=" + sat_values["slider-discrete"]
            if sat_values.get("slider-pansharpening"):
                if sat_values["slider-pansharpening"]:
                    if more_data_slider:
                        more_data_slider += "&PANSHARPENING=True"
                    else:
                        more_data_slider += "?PANSHARPENING=True"
            land_viewer_data.append({"sat": sat_values["slider-s"],
                                     "band_c": band_combination_slider,
                                     "id": sat_values["slider-id"],
                                     "more": more_data_slider})
        sat_dict = {
            "Landsat8": "landsat8",
            "Landsat7": "landsat7",
            "Sentinel2": "sentinel2",
            "Sentinel1": "sentinel1",
            "Modis": "modis",
            "CBERS4(MUX)": "cbers4",
            "CBERS4(PAN10M)": "cbers4",
            "CBERS4(PAN5M)": "cbers4",
            "CBERS4(AWFI)": "cbers4"
        }
        response = []
        for current_data in land_viewer_data:
            if sat_dict[current_data["sat"]] in ("landsat7", "landsat8"):
                search_value = "productID"
            else:
                search_value = "sceneID"
            search_dict = {
                "search": {
                    search_value: current_data["id"],
                },
                "fields": [
                    "sceneID", "awsPath", "cloudCoverage", "satelliteName",
                    "horizontalTileNumber", "verticalTileNumber", "thumbnail",
                    "BeginningDateTime", "productID", "date", "dataGeometry"
                ]
            }
            resp_image = requests.post("http://lms.eosda.com/search/v2/" +
                                       sat_dict[current_data["sat"]],
                                       data=json.dumps(
                                           search_dict)).json()["results"][0]

            if sat_dict[current_data["sat"]] in ("landsat7", "landsat8"):
                scene_id = scene_name = resp_image["productID"]
                image_ulr = resp_image["thumbnail"]
            elif sat_dict[current_data["sat"]] == "sentinel2":
                scene_id = resp_image['awsPath'][5:]
                scene_name = resp_image['sceneID']
                image_ulr = "http://sentinel-s2-l1c.s3.amazonaws.com/" \
                            + resp_image["awsPath"] + "/preview.jpg"
            elif sat_dict[current_data["sat"]] == "sentinel1":
                scene_id = resp_image['sceneID']
                scene_name = resp_image['sceneID']
                image_ulr = "https://render.eosda.com/S1/thumb/" \
                            + scene_name + ".png"
                resp_image["satelliteName"] = "Sentinel-1"
                resp_image["cloudCoverage"] = 0.0
                resp_image["date"] = parser.parse(resp_image["date"]).date()\
                    .strftime('%Y-%m-%d')
            elif sat_dict[current_data["sat"]] == "modis":
                h_tile = int(resp_image["horizontalTileNumber"])
                if len(str(h_tile)) == 1:
                    h_tile = "0" + str(h_tile)
                else:
                    h_tile = str(h_tile)
                v_tile = int(resp_image["verticalTileNumber"])
                if len(str(v_tile)) == 1:
                    v_tile = "0" + str(v_tile)
                else:
                    v_tile = str(v_tile)
                scene_id = h_tile + "/" + v_tile + "/" + \
                           resp_image["sceneID"].split(".")[1][1:] + "/" + \
                           resp_image["sceneID"].split(".")[-1]
                scene_name = resp_image['sceneID']
                image_ulr = resp_image["thumbnail"]
                resp_image["date"] = parser.parse(
                    resp_image["BeginningDateTime"]).date()\
                    .strftime('%Y-%m-%d')
            elif sat_dict[current_data["sat"]] == "cbers4":
                scene_id = scene_name = resp_image["sceneID"]
                image_ulr = "http://render.eosda.com/CBERS4/thumb/" + \
                            scene_id + ".jpeg"
                resp_image["cloudCoverage"] = 0.0
            for i in bands_dictionary[resp_image["satelliteName"]].items():
                current_data["band_c"] = current_data["band_c"].replace(i[0],
                                                                        i[1])
            sat_image = SatelliteImage.objects.create(
                event=event,
                cloud_coverage=resp_image["cloudCoverage"],
                satellite_name=resp_image["satelliteName"],
                scene_id=scene_id,
                scene_name=scene_name,
                date=resp_image["date"],
                image_url=image_ulr,
                data_geometry="{'type': 'Polygon', 'coordinates': " + str(
                    resp_image['dataGeometry']['coordinates']) + "}",
                band_combinations=current_data["band_c"],
                any_params=current_data["more"]
            )
            response.append(sat_image)
        return response
    except KeyError:
        return None
