from rest_framework import serializers
from .models import SatelliteImage


class SatelliteImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = SatelliteImage
        fields = '__all__'
