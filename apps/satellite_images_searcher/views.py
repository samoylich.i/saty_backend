from datetime import datetime
import re

from rest_framework.response import Response
from rest_framework import generics
from django.db.models import Q
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.status import (HTTP_400_BAD_REQUEST,)

from .serializers import SatelliteImagesSerializer
from .models import Event
from .models import SatelliteImage
from .search_scene import _get_queryset
import requests


class ImagesList(generics.ListCreateAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        events = Event.objects.filter(approved_status=True)
        return SatelliteImage.objects.filter(
            event__in=events, approved_status=True).order_by("id")


class ImagesDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        events = Event.objects.filter(approved_status=True)
        return SatelliteImage.objects.filter(event__in=events)


class ImagesByEvent(generics.ListAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        number = self.kwargs['number']

        # for sort by popularity
        try:
            event = Event.objects.get(pk=int(number))
            return SatelliteImage.objects.filter_by_event_type(event)\
                .filter(approved_status=True).order_by('-date')
        except Event.DoesNotExist:
            return SatelliteImage.objects.none()


class ImagesForPhotos(generics.ListAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):

        number = self.kwargs['number']
        try:
            Event.objects.get(pk=int(number))
            return SatelliteImage.objects.filter(
                event=number, approved_status=True,
                image_status=True).order_by('-date')
        except Event.DoesNotExist:
            return SatelliteImage.objects.none()


class ImagesByEventGive2Images(generics.ListAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        number = self.kwargs['number']
        try:
            event = Event.objects.get(pk=int(number))
            return SatelliteImage.objects.get_twice(event).order_by('date')
        except (Event.DoesNotExist):
            return SatelliteImage.objects.filter(event=number)


class ImagesByScene(generics.ListAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        first_sc = self.kwargs['first_image_id']
        last_sc = self.kwargs['last_image_id']
        return SatelliteImage.objects.filter(
            Q(id=first_sc) | Q(id=last_sc), event=self.kwargs['event_id'])


class ImagesManyFiltering(generics.ListAPIView):
    serializer_class = SatelliteImagesSerializer

    def get_queryset(self):
        images = SatelliteImage.objects.filter_by_event_type(
            Event.objects.get(pk=int(self.kwargs['event_id']))).filter(
            approved_status=True)
        return (SatelliteImage.objects.many_filters(
                    images_data=images,
                    type_name=self.kwargs['satellite_name'],
                    start_date=self.kwargs['start_date'],
                    end_date=self.kwargs['end_date'],
                    cloud_coverage=self.kwargs['cloud_coverage']))


class AddNewImages(APIView):

    def get(self, request, current_date, lon, lat, satellite, direction):
        return Response({"results": _get_queryset(
            current_date, lon, lat, satellite, direction)})


def images_date_range(request, number):
    try:
        images = SatelliteImage.objects.filter(
            event=number, approved_status=True)
        min_date = max_date = images[0].date
        for im in images:
            if im.date < min_date:
                min_date = im.date
            if im.date > max_date:
                max_date = im.date
        return JsonResponse({"Minimal images date": min_date,
                             "Maximum images date": max_date})
    except IndexError:
        return JsonResponse({"Event's images": "not found"})


class DeleteWrongScene(APIView):

    def post(self, request):
        try:
            scene_id = request.data["scene_id"]
            if request.data["scene_name"].find("LC08") != -1:
                pre = "L8"
                band = "B4"
            elif request.data["scene_name"].find("LE07") != -1:
                pre = "L7"
                band = "B3"
            elif request.data["scene_name"].find("S2") != -1:
                pre = "S2"
                band = "B04"
                scene_id = request.data["scene_id"][1:]
            elif request.data["scene_name"].find("S1") != -1:
                pre = "S1"
                band = "VV"
            elif request.data["scene_name"].find("MCD43A4") != -1:
                pre = "MODIS"
                band = "B01"
            elif request.data["scene_name"].find("CBERS_4_MUX") != -1:
                pre = 'CBERS4MUX'
                band = "BAND7"
            elif request.data["scene_name"].find("CBERS_4_AWFI") != -1:
                pre = 'CBERS4AWFI'
                band = "BAND15"
            elif request.data["scene_name"].find("CBERS_4_PAN10M") != -1:
                pre = 'CBERS4PAN10M'
                band = "BAND4"
            elif request.data["scene_name"].find("CBERS_4_PAN5M") != -1:
                pre = 'CBERS4PAN5M'
                band = "BAND1"
            else:
                return Response("Wrong data", status=HTTP_400_BAD_REQUEST)
            response = requests.get(
                "https://render.eosda.com/" + pre + "/ir/" + scene_id + "/" + band)
            if response.status_code == 200:
                return Response("Scene is true")
            elif response.status_code == 422:
                SatelliteImage.objects.filter(
                    scene_name=request.data["scene_name"]).delete()
                return Response("The wrong sat-image was deleted")
        except KeyError:
            return Response("Wrong parameters", status=HTTP_400_BAD_REQUEST)
