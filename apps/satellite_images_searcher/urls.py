from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view
from . import views

schema_view = get_swagger_view(title='Satellite images')

urlpatterns = [
    url(r'^$', schema_view),
    url(r'^sat-images/$', views.ImagesList.as_view(),
        name='sat-images-list'),
    url(r'^sat-images/detail/(?P<pk>\d+)/$', views.ImagesDetail.as_view(),
        name='sat-images_detail'),
    url(r'^sat-images/event/(?P<number>\d+)/$', views.ImagesByEvent.as_view(),
        name='sat-images-by_event'),
    url(r'^sat-images/event/(?P<number>\d+)&more/$',
        views.ImagesByEventGive2Images.as_view(),
        name='two_sat-images_by_event'),
    url(r'^sat-images/event_by_sceneid/(?P<event_id>\d+)/'
        r'(?P<first_image_id>\d+)/(?P<last_image_id>\d+)/$',
        views.ImagesByScene.as_view(),
        name='sat-images_by_scene-id'),
    url(r'^sat-images/event/(?P<number>\d+)/dates/$', views.images_date_range,
        name='sat-images_date_range'),
    url(r'^sat-images/current_event_data/(?P<event_id>\d+)/'
        r'(?P<satellite_name>.+)/(?P<cloud_coverage>\d+)/(?P<start_date>.+)/'
        r'(?P<end_date>.+)/$', views.ImagesManyFiltering.as_view(),
        name='sat-images_many_filter'),
    url(r'^sat-images/add_images/(?P<current_date>.+)/(?P<lon>.+)/'
        r'(?P<lat>.+)/(?P<satellite>.+)/(?P<direction>\d)/$',
        views.AddNewImages.as_view(),
        name='dynamic_loading_sat-images'),
    url(r'^sat-images/wrong/$', views.DeleteWrongScene.as_view(),
        name='delete_wrong_scene'),
    url(r'^sat-images/photo_by_event/(?P<number>\d+)/$', views.ImagesForPhotos.as_view()),
]
