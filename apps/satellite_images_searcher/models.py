from django.contrib.gis.db import models
from apps.event_viewer.models import Event
from django.db.models import Q
import re
from datetime import datetime


class Check_Satellite(models.Manager):

    def get_twice(self, event):
        SATELLITES = ('Sentinel-2A', 'Sentinel-2B',
                      'CBERS-4', 'landsat-8', 'landsat-7')
        sat_images = super(Check_Satellite, self).get_queryset().filter(
            event=event, approved_status=True, satellite_name__in=SATELLITES,
            cloud_coverage__lte=40).order_by('-date')

        after = before = bool()
        for s in SATELLITES:
            after = sat_images.filter(date__gte=event.start_date,
                                      satellite_name=s).last()
            if after: break
        if not after:
            for s in SATELLITES:
                after = sat_images.filter(date__lt=event.start_date,
                                          satellite_name=s).first()
                if after: break
        for s in SATELLITES:
            before = sat_images.filter(date__lt=event.start_date,
                                       satellite_name=s).first()
            if before and after != before: break
        if after and before:
            return super(Check_Satellite, self).get_queryset().filter(
                Q(pk=before.pk) |
                Q(pk=after.pk), event=event)
        elif sat_images.count() > 1:
            return sat_images.order_by("date")[:2]
        elif sat_images.count() == 1:
            return [sat_images[0], sat_images[0]]
        else:
            return super(Check_Satellite, self).get_queryset().none()

    def filter_by_event_type(self, event):
        EVENT_TYPE_SATELLITES = {"Wildfire": ['Sentinel-2A', 'Sentinel-2B',
                                              'CBERS-4', 'landsat-8',
                                              'landsat-7', 'modis'],
                                 "Flood": ['Sentinel-2A', 'Sentinel-2B',
                                           'CBERS-4', 'landsat-8', 'landsat-7',
                                           'modis', 'Sentinel-1'],
                                 "Volcano": ['Sentinel-2A', 'Sentinel-2B',
                                             'CBERS-4', 'landsat-8',
                                             'landsat-7', 'modis'],
                                 "Earthquake": ['Sentinel-2A', 'Sentinel-2B',
                                                'CBERS-4', 'landsat-8',
                                                'landsat-7', 'modis',
                                                'Sentinel-1'],
                                 "Cyclone": ['Sentinel-2A', 'Sentinel-2B',
                                             'CBERS-4', 'landsat-8',
                                             'landsat-7', 'modis',
                                             'Sentinel-1'],
                                 "Drought": ['Sentinel-2A', 'Sentinel-2B',
                                             'CBERS-4', 'landsat-8',
                                             'landsat-7', 'modis',
                                             'Sentinel-1'],
                                 "Landslide": ['Sentinel-2A', 'Sentinel-2B',
                                               'CBERS-4', 'landsat-8',
                                               'landsat-7', 'modis',
                                               'Sentinel-1'],
                                 "Manmade": ['Sentinel-2A', 'Sentinel-2B',
                                             'CBERS-4', 'landsat-8',
                                             'landsat-7', 'modis',
                                             'Sentinel-1'],
                                 "Water Color": ['Sentinel-2A', 'Sentinel-2B',
                                                 'CBERS-4', 'landsat-8',
                                                 'landsat-7', 'modis',
                                                 'Sentinel-1'],
                                 "Other": ['Sentinel-2A', 'Sentinel-2B',
                                           'CBERS-4', 'landsat-8', 'landsat-7',
                                           'modis', 'Sentinel-1']}
        return super(Check_Satellite, self).get_queryset().filter(
            event=event,
            satellite_name__in=EVENT_TYPE_SATELLITES[event.event_type])

    def many_filters(self, images_data, type_name,
                     start_date, end_date, cloud_coverage):
        min_date = images_data.order_by("date").first().date
        max_date = images_data.order_by("date").last().date

        if type_name != "all":
            types = type_name.split('&')
            images_data = images_data.filter(satellite_name__in=types)

        if re.match(r'[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])',
                    start_date):
            sd = (datetime.strptime(start_date,
                                    "%Y-%m-%d")).date()
            if sd >= min_date:
                start_date = sd
            else:
                start_date = min_date
        else:
            start_date = min_date

        if re.match(r'[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])',
                    end_date):
            ed = (datetime.strptime(end_date,
                                    "%Y-%m-%d")).date()
            if ed <= max_date:
                end_date = ed
            else:
                end_date = max_date
        else:
            end_date = max_date
        return (images_data.filter(
            date__range=(start_date, end_date),
            cloud_coverage__lte=cloud_coverage).order_by("-date"))


class SatelliteImage(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    image_status = models.BooleanField(default=False)
    approved_status = models.BooleanField(default=True)
    satellite_name = models.CharField(max_length=50)
    cloud_coverage = models.FloatField()
    scene_name = models.CharField(max_length=156)
    scene_id = models.CharField(max_length=156)
    date = models.DateField()
    image_url = models.URLField(max_length=300)
    data_geometry = models.PolygonField(blank=True, null=True)
    band_combinations = models.CharField(blank=True, null=True,
                                         max_length=150, default=None)
    any_params = models.CharField(blank=True, null=True,
                                  max_length=200, default=None)
    nbr_threshold = models.FloatField(default=0.21)

    objects = Check_Satellite()


    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return ('Event: ' + str(self.event) + ' Satellite:' +
                str(self.satellite_name) + ' Scene:' + str(self.scene_id))


