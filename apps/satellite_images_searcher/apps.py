from django.apps import AppConfig


class SatelliteImagesSearcherConfig(AppConfig):
    name = 'apps.satellite_images_searcher'
    verbose_name = "Satellite Images Searcher"
