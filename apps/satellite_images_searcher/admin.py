from django.contrib import admin
from apps.satellite_images_searcher.models import SatelliteImage
from rangefilter.filter import DateRangeFilter
from django.utils.html import format_html


# Register your models here.
class SatelliteImagesAdmin(admin.ModelAdmin):
    def image_tag(self):
        return format_html('''
            <head>
            <style type="text/css">
                .foc {{
                width:300px;
                cursor:pointer;
                display:inline;
                }}
                .foc:focus {{
                width: auto;
                z-index: 150;
                }}
            </style>
            </head>
            <body>
                <img border="0" src="''' + self.image_url + '''" class="foc" tabindex="1"/>
            </body>
            ''')


    image_tag.short_description = 'Image'
    list_display = ('event', image_tag, 'scene_name',
                    'satellite_name', 'cloud_coverage',
                    'date', 'approved_status',)
    list_editable = ('approved_status',)
    date_hierarchy = 'event__adding_date'
    list_filter = ('event__event_type', 'event__approved_status',
                   'satellite_name', ('event__start_date', DateRangeFilter),
                   'event__title')
    search_fields = ['event__title', 'event__place']
    list_per_page = 30
    save_on_top = True


admin.site.register(SatelliteImage, SatelliteImagesAdmin)
