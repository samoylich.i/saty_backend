from django.contrib import admin
from django.conf.urls import url, include

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'', include('apps.event_viewer.urls')),
    url(r'', include('apps.satellite_images_searcher.urls')),
]
